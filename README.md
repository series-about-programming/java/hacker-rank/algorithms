# SERIES ABOUT HACKER RANK ALGORITHMS

- Series độc quyền về giải các bài toán thuật toán trên hackerrank
- Nếu các bạn thấy hay có thể đánh dấu cho mình 1 sao + chia sẽ đến mọi người
- Nếu có phần thắc mắc hay mình gặp sai sót trong phần nội dung, mong các bạn đóng góp ý kiến đến email **`thonghoangpham123@gmail.com`**, Mình xin cảm ơn.

## Các bài viết

| Bài viết                                         | Link              |
| ------------------------------------------------ | ----------------- |
| Day 1: Simple Array Sum                          | [Link](day001.md) |
| Day 2: Compare The Triplets                      | [Link](day002.md) |
| Day 3: 2 Đường Chéo Ma Trận Vuông                | [Link](day003.md) |
| Day 4: Staircase                                 | [Link](day004.md) |
| Day 5: Mini Max Sum                              | [Link](day005.md) |
| Day 6: Số Lần Xuất Hiện Của Phần Tử Max          | [Link](day006.md) |
| Day 7: Kiểm tra sum subarray có bằng số chỉ định | [Link](day007.md) |
